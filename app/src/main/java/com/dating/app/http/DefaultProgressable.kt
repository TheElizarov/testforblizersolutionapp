package com.dating.app.http

import android.app.Activity
import android.app.ProgressDialog
import android.os.Handler

open class DefaultProgressable(val activity: Activity) : Progressable {

    private var progressDialog: ProgressDialog? = null

    private var message = "Waiting.."
    private var cancelable = true

    open fun isCancelable() = true

    override fun begin() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(activity)
        }
        progressDialog?.let {
            it.setMessage(message)
            it.show()
        }
    }

    override fun end() {
        Handler().post { dismiss() }
    }

    fun setCancelableByUser(cancelable: Boolean): DefaultProgressable {
        this.cancelable = cancelable
        return this
    }

    fun setMessage(message: String): DefaultProgressable {
        this.message = message
        return this
    }

    private fun dismiss() {
        message = "Waiting.."
        try {
            progressDialog?.dismiss()
        } catch (ignored: Exception) {
        }
    }
}