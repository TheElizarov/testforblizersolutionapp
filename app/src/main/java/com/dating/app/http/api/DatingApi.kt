package com.dating.app.http.api

import com.dating.app.BuildConfig
import com.dating.app.model.pojo.Token
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.Headers
import retrofit2.http.POST

interface DatingApi {
    @Headers(
        "Key: ${BuildConfig.API_KEY}"
    )
    @FormUrlEncoded
    @POST("${Api.Controllers.DATING}/${Api.Methods.TOKEN}")
    fun token(@Field("UserName", encoded = false) userName: String,
              @Field("Password", encoded = false) password: String,
              @Field("grant_type") grantType: String = "password",
              @Field("accesstoken") accessToken: String = "",
              @Field("facebookId") facebookId: String = ""): Observable<Token>

}