package com.dating.app.http.api.mock

import com.dating.app.http.api.DatingApi
import com.dating.app.model.pojo.Token
import io.reactivex.Observable

/**
 * Use for mock response from server
 * Usually using raw resource for stub file as mock response
 */
class DatingApiMock : DatingApi {
    override fun token(userName: String, password: String, grantType: String, accessToken: String, facebookId: String)
            = Observable.just(Token())

}