package com.dating.app.http

interface Progressable {
    fun begin()

    fun end()
}