package com.dating.app.http.api

import com.dating.app.model.api.UsersApiModel
import com.dating.app.model.pojo.User
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface UsersApi {
    /**
     * urlPathString
     * String
     * "https://thaiBlazing.com/dating/api/GetUsers?
     * gender=2&
     * page=1&
     * genderLookingInto=1&
     * countryID=211&
     * thaiCityID=0&
     * minAge=18&maxAge=50&minWeight=40&maxWeight=100&minTall=140&maxTall=200&searchQuery=c
     * an you make sure you have these values becuse you should have more users
     */
    @Headers(
            "Content-Type: application/json",
            "Accept: application/json"
    )
    @GET("${Api.Controllers.DATING}/${Api.Methods.GET_USERS}")
    fun users(@Query("gender") gender: Int = User.Gender.OTHER.value,
              @Query("page") page: Int = 1,
              @Query("genderLookingInto") genderLookingInto: Int = User.GenderLookingInfo.ZERO.value,
              @Query("countryID") countryId: Int = 211,
              @Query("thaiCityID") thaiCityId: Int = 0,
              @Query("minAge") minAge: Int = 18,
              @Query("maxAge") maxAge: Int = 100,
              @Query("minWeight") minWeight: Int = User.Weight.MIN.value,
              @Query("maxWeight") maxWeight: Int = User.Weight.MAX.value,
              @Query("minTall") minTall: Int = User.Tall.MIN.value,
              @Query("maxTall") maxTall: Int = User.Tall.MAX.value,
              @Query("searchQuery") searchQuery: String = " "): Observable<UsersApiModel>
}