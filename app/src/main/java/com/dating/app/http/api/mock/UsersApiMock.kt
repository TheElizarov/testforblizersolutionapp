package com.dating.app.http.api.mock

import com.dating.app.http.api.UsersApi
import com.dating.app.model.api.UsersApiModel
import io.reactivex.Observable

/**
 * Use for mock response from server
 * Usually using raw resource for stub file as mock response
 */
class UsersApiMock : UsersApi {
    override fun users(gender: Int, page: Int, genderLookingInto: Int, countryId: Int, thaiCityId: Int, minAge: Int, maxAge: Int, minWeight: Int, maxWeight: Int, minTall: Int, maxTall: Int, searchQuery: String)
            = Observable.just(UsersApiModel())

}