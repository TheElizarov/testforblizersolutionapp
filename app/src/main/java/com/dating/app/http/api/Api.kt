package com.dating.app.http.api

import com.dating.app.App
import com.dating.app.BuildConfig
import com.dating.app.http.api.mock.DatingApiMock
import com.dating.app.http.api.mock.UsersApiMock
import com.dating.app.utils.get
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Api {
    const val URL = "https://${BuildConfig.HOST}:${BuildConfig.PORT}"
    const val CONNECTION_TIMEOUT_IN_SECONDS = 60L

    object Versions {
        const val V1 = "v1"
        const val V2 = "v2"
        const val V3 = "v3"
        const val V4 = "v4"
        const val V5 = "v5"
        const val CURRENT = V1
    }

    object Controllers {
        const val DATING = "dating/api"
        const val PHOTOS = "${Versions.V1}/Photos"
    }

    object Methods {
        const val TOKEN = "token"
        const val GET_USERS = "GetUsers"
    }

    lateinit var dating: DatingApi
    lateinit var users: UsersApi

    fun init(isDatingMock: Boolean = false,
             isPhotoMock: Boolean = false): Api {
        dating = if (isDatingMock) DatingApiMock() else build(DatingApi::class.java)
        users = if (isPhotoMock) UsersApiMock() else build(UsersApi::class.java)
        return this
    }

    fun <API> build(api: Class<API>): API {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = if (BuildConfig.DEBUG)
            HttpLoggingInterceptor.Level.BODY
        else
            HttpLoggingInterceptor.Level.NONE

        val cookiesInterceptor = Interceptor { chain ->
            val builder = chain.request().newBuilder()
            val token = App.instance.get()?.value ?: ""
            if (!token.isNullOrEmpty()) {
                builder.addHeader("Authorization", "Bearer $token")
            }
            chain.proceed(builder.build())
        }

        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(CONNECTION_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .addInterceptor(cookiesInterceptor)
                .addInterceptor(logInterceptor)
                .build()

        val gson = GsonBuilder()
                .setPrettyPrinting()
                .create()

        val gsonConverterFactory = GsonConverterFactory.create(gson)

        val retrofit = Retrofit.Builder()
                .baseUrl(URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(gsonConverterFactory)
                .build()

        return retrofit.create(api)
    }

}