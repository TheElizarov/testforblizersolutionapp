package com.dating.app.utils

import android.content.Context
import android.support.v7.app.AlertDialog
import com.dating.app.http.Progressable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun <T> Observable<T>.applySchedulers() : Observable<T>
        = this
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.applySchedulers() : Single<T>
        = this
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

fun <Result> Observable<Result>.request(context: Context? = null,
                                        progressable: Progressable? = null,
                                        onError: ((e: Throwable) -> Unit)? = null,
                                        onSuccess: ((result: Result?) -> Unit)? = null): Disposable {
    progressable?.begin()
    return applySchedulers()
            .subscribe({
                onSuccess?.invoke(it)
                progressable?.end()
            }, {
                val error = it
                progressable?.end()
                onError?.invoke(error) ?: context?.let {
                    AlertDialog.Builder(it)
                            .setMessage("Ohh.. somethings wrong! please, repeat your request later")
                            .setPositiveButton("OK") { _, _ -> }
                            .show()
                }
            })
}

