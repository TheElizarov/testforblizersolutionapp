package com.dating.app.utils

import android.content.Context
import com.dating.app.BuildConfig
import com.dating.app.model.pojo.Token
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

const val PREFS = "token_prefs"
const val TAG_TOKEN = "tag_token"

/**
 * extension for context for saving [Token] in local storage
 */
fun Context.set(token: Token) = getSharedPreferences(PREFS, Context.MODE_PRIVATE).apply {
    edit().putString(
            TAG_TOKEN,
            Gson().toJson(token)
    ).apply()
}

fun Context.get(): Token? {
    getSharedPreferences(PREFS, Context.MODE_PRIVATE).apply {
        val token = getString(TAG_TOKEN, "")
        return if (token != null && token.isNotEmpty())
            Gson().fromJson<Token>(
                    token,
                    object : TypeToken<Token>() {}.type
            )
        else
            null
    }
}

fun Context.hasToken() = get()?.value?.isNotEmpty() ?: false

fun Context.debug(action: Context.() -> Unit) {
    if (BuildConfig.DEBUG) {
        action()
    }
}

