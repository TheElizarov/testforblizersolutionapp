package com.dating.app.utils

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import com.dating.app.http.Progressable
import com.squareup.picasso.Picasso

fun Activity.displayError(message: String) = AlertDialog.Builder(this)
        .setMessage(message)
        .setPositiveButton("OK") { _, _ -> }
        .show()

fun Activity.displayMessage(message: String, okListener: (() -> Unit)? = null) = AlertDialog.Builder(this)
        .setMessage(message)
        .setPositiveButton("OK") { _, _ -> okListener?.invoke() }
        .show()

fun ImageView.fromNetwork(url: String,
                          placeHolderResId: Int = -1,
                          errorResId: Int = -1,
                          enableLogInDebug: Boolean = false) {
    context.debug {
        if (enableLogInDebug)
            Log.v("ImageView.fromNetwork", url)
    }
    with(Picasso.get().load(url)) {
        if (placeHolderResId != -1) placeholder(placeHolderResId)
        if (errorResId != -1) error(errorResId)
        into(this@fromNetwork)
    }

}

fun SwipeRefreshLayout.getProgressable() = object : Progressable {
    override fun begin() {
        isRefreshing = true
    }

    override fun end() {
        isRefreshing = false
    }

}

fun EditText.filter(action: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(p0: Editable?) {
            p0?.let {
                action(it.toString())
            }
        }

    })
}

fun RecyclerView.pagination(action:() -> Unit) {
    addOnScrollListener(RecyclerScrollableController(object : RecyclerScrollableController.OnLastItemVisibleListener {
        override fun onLastItemVisible() = action.invoke()
    }))
}

fun Fragment.isInternetAvailable()  = context?.let {
    val connectivityManager = it.getSystemService(Context.CONNECTIVITY_SERVICE)
    return if (connectivityManager is ConnectivityManager) {
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
        networkInfo?.isConnected ?: false
    } else false
} ?: false

/**
 * Use it for detect pagination logic for [RecyclerView]
 */
class RecyclerScrollableController(val listener: OnLastItemVisibleListener?) : RecyclerView.OnScrollListener() {

    private var previousTotal = 0
    private var loading = true

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = recyclerView.childCount
        val layoutManager = recyclerView.layoutManager as LinearLayoutManager?
        val totalItemCount = layoutManager!!.itemCount
        val firstVisibleItem = layoutManager.findFirstVisibleItemPosition()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        val isLastItemVisible = totalItemCount - visibleItemCount <= firstVisibleItem + VISIBLE_THRESHOLD
        if (isLastItemVisible) {
            listener?.onLastItemVisible()
            loading = true
        }
    }

    interface OnLastItemVisibleListener {

        fun onLastItemVisible()

        companion object {

            val STUB: OnLastItemVisibleListener = object : OnLastItemVisibleListener {
                override fun onLastItemVisible() {}
            }
        }
    }

    companion object {
        val VISIBLE_THRESHOLD = 5
    }
}
