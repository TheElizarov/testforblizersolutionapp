package com.dating.app.model.viewmodel

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.dating.app.http.Progressable
import com.dating.app.model.pojo.Token
import com.dating.app.repository.TokenRepository
import com.dating.app.utils.get
import com.dating.app.utils.hasToken

class TokenViewModel : BaseViewModel() {
    var tokenLD = MutableLiveData<Token>()
    var errorLD = MutableLiveData<Throwable>()
    private val repository = TokenRepository()

    override fun subscribeTo(owner: LifecycleOwner) {
        super.subscribeTo(owner)
        owner.lifecycle.addObserver(repository)
    }

    fun auth(context: Context? = null,
             progressable: Progressable? = null,
             user: String,
             password: String) = repository.token(
            context,
            progressable,
            user,
            password,
            onSucces = {
                tokenLD.value = it
            },
            onError = {
                errorLD.value = it
            }
    )

    fun checkLocalToken(context: Context) {
        if (context.hasToken()) tokenLD.value = context.get()
        else errorLD.value = Exception("Require authorization")
    }
}