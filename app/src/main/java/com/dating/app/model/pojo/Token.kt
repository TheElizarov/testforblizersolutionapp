package com.dating.app.model.pojo

import com.google.gson.annotations.SerializedName

/**
 * Use for authorized api requests
 * Client must use [TokenRepository]
 * Format from api
 * {
        "access_token": "LKzvcbkl3EtSuVSEOWGor1pAEa5p9vHMxmsJsKppjAGpmE6vE_Av7M1e-2YMY9q215Gko33piRMg7_82W7UUuGHxca7d6rqQmxuqULjisBkXpfL1avHS2P6vLOYieDt9GVYn79h4yEU8iUrsyzduy3AzSkZEsWAVEbNC-irIxFEe08qr7BGnRMvoUnWk7Fi_is-iYtA4t96EFA_nc9-LFZU_QYnBXlf8VIEGVYRjM3jc-4eynouAaLYnZgUpQIwDQQ0CGBL9q_tQdkcjbFXZn2tI8Fk",
        "token_type": "bearer",
        "expires_in": 8639999,
        "UserID": "691",
        "AccountNeedsVerification": "1",
        ".issued": "Fri, 21 Dec 2018 15:11:31 GMT",
        ".expires": "Sun, 31 Mar 2019 15:11:31 GMT"
    }
 */

data class Token(@SerializedName("access_token") val value: String = "",
                 @SerializedName("token_type") val type: String = "",
                 @SerializedName("expires_in") val expiresIn: Long = -1L,
                 @SerializedName("UserID") val userId: String = "",
                 @SerializedName("AccountNeedsVerification") val isNeedVerification: Int = -1,
                 @SerializedName(".issued") val dateIssued: String = "",
                 @SerializedName(".expires") val dateExpires: String = "")