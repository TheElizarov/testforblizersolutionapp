package com.dating.app.model.pojo

import com.google.gson.annotations.SerializedName

/**
 * Example response from server
 * "Paging":{"PageNo":1,"PageSize":20,"PageCount":1,"TotalRecordCount":14}}}
 */
data class Paging(@SerializedName("PageNo") val no: Int = 1,
                  @SerializedName("PageSize") val size: Int = DEFAULT_PAGE_SIZE,
                  @SerializedName("PageCount") val count: Int = 1,
                  @SerializedName("TotalRecordCount") val total: Int = DEFAULT_PAGE_SIZE) {

    fun isAvailable() = no < count

    companion object {
        const val DEFAULT_PAGE_SIZE = 20
    }
}