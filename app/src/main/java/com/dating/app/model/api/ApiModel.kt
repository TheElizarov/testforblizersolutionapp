package com.dating.app.model.api

/**
 * Use it for http calls
 * Base api model for wrap pojo model as body or response
 */
open class ApiModel