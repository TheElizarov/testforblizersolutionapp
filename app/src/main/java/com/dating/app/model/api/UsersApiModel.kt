package com.dating.app.model.api

import com.dating.app.model.pojo.Paging
import com.dating.app.model.pojo.User
import com.google.gson.annotations.SerializedName

class UsersApiModel(val errorCode: Int = 0,
                    val stat: String = "",
                    val statusMessage: String = "",
                    @SerializedName("Users") val users: Data = Data()) : ApiModel() {
    data class Data(@SerializedName("Data") val data: List<User> = listOf(),
                    @SerializedName("Paging") val paging: Paging = Paging())
}