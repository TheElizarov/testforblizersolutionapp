package com.dating.app.model.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.content.Context
import com.dating.app.App
import com.dating.app.http.Progressable
import com.dating.app.model.pojo.User
import com.dating.app.utils.request

/**
 * Use it for search users
 */
class UsersViewModel : BaseViewModel() {
    val usersLD = MutableLiveData<List<User>>()
    val errorLD = MutableLiveData<Exception>()
    /**
     * page for pagination request
     */
    private var page: Int = 1
    private var isRequestAvailable = true
    /**
     * time last update
     * init with delay, it need for exception update after init view model
     */
    private var timeForUserLastUpdated = System.currentTimeMillis() + INTERVAL_FOR_UPDATE_USERS_LIST_IN_MILLS

    fun getUsers(context: Context, progressable: Progressable? = null) {
        if (isRequestAvailable) {
            isRequestAvailable = false
            request(
                    App.instance.api.users.users(page = page).request(
                            context,
                            progressable,
                            onError = { e ->
                                isRequestAvailable = true
                                errorLD.value = Exception(e.localizedMessage)
                                timeForUserLastUpdated = System.currentTimeMillis()
                            },
                            onSuccess = {
                                it?.let {
                                    usersLD.value = it.users.data
                                    timeForUserLastUpdated = System.currentTimeMillis()
                                    /**
                                     * increment page if previos request return data
                                     * if previos request is empty, it is say,
                                     * that was last page with data
                                     */
                                    if (it.users.paging.isAvailable()) {
                                        ++page
                                        isRequestAvailable = true
                                    }
                                }
                            })
            )
        }
    }

    fun resetPagination() {
        page = 1
        isRequestAvailable = true
    }

    /**
     * Checks time from last update
     */
    fun isNeedUpdate() = System.currentTimeMillis() - timeForUserLastUpdated > INTERVAL_FOR_UPDATE_USERS_LIST_IN_MILLS

    companion object {
        const val INTERVAL_FOR_UPDATE_USERS_LIST_IN_MILLS = 60_000L
    }

}