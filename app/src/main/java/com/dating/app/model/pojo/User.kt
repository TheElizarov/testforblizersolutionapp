package com.dating.app.model.pojo

import com.dating.app.http.api.Api
import com.google.gson.annotations.SerializedName

/**
 * Model for describe user info
 * For test app parse only important fields
 * Bellow example full json entity
 */
class User(@SerializedName("UserID") val id: Int,
           @SerializedName("UserName") val name: String,
           @SerializedName("FirstName") val firstName: String,
           @SerializedName("LastName") val lastName: String,
           @SerializedName("IsOnline") val isOnlineCode: Int,
           @SerializedName("CountryName") val countryName: String,
           @SerializedName("City") val city: String,
           @SerializedName("Age") val age: Int,
           @SerializedName("PhotoList") val photos: List<Photo>) {
    val profileUrl get() =  "${Api.URL}/${Api.Controllers.PHOTOS}/$id/profile.jpg"
    val isOnline get() = isOnlineCode == 1

    data class Photo(@SerializedName("PhotoID") val id: Int,
                     @SerializedName("UserID") val userId: Int,
                     @SerializedName("PhotoURL") val fromServerUrl: String?,
                     @SerializedName("CreatedDate") val createdDate: String,
                     @SerializedName("Order") val order: Int) {
        private val baseUserUrl get() =  "${Api.URL}/${Api.Controllers.PHOTOS}/$userId"
        private val basePhotoUrl get() =  "$baseUserUrl/$id"
        val normalUrl get() = "${basePhotoUrl}_n.jpg"
        val smallUrl get() = "${basePhotoUrl}_s.jpg"
        val profileUrl get() = "$baseUserUrl/profile.jpg"
    }

    enum class Gender(val value: Int) {
        MALE(0), FEMALE(1), TRANS(2), OTHER(3)
    }

    enum class GenderLookingInfo(val value: Int) {
        ZERO(0), ONE(1), SECOND(2)
    }

    enum class Weight(val value: Int) {
        MIN(4), MAX(100)
    }

    enum class Tall(val value: Int) {
        MIN(140), MAX(200)
    }
}

/**
 * {
    "UserID": 678,
    "QBUserID": "61116051",
    "QBPassword": null,
    "UserName": null,
    "FirstName": "Amour",
    "LastName": null,
    "Email": null,
    "ChatID": null,
    "Gender": 1,
    "CountryID": 211,
    "CountryName": "Thailand",
    "City": "Bangkok",
    "Password": null,
    "LON": null,
    "LAT": null,
    "Status": "Oooo",
    "IsSuper": false,
    "IsOnline": 0,
    "IsActive": true,
    "CreatedDate": "2018 - 09 - 21T15: 00:17Z",
    "LastDateAccessed": "2018 - 10 - 12T19: 47:02Z",
    "TransDate": null,
    "BirthDate": "2000 - 08 - 21T07: 00:00Z",
    "LoginType": "manual",
    "FBtoken": 0,
    "Weight": 40,
    "Tall": 140,
    "GenderLookingTo": 2,
    "ThaiCityID": 1,
    "RegistrationStepsCompleted": 3,
    "HasProfilePhoto": true,
    "IsApproved": true,
    "Age": 18,
    "PhotoID": 1076,
    "TLikes": 0,
    "TViews": 0,
    "FBProfilePicLink": 0,
    "FBProfileID": null,
    "ThaiCityName": "Bangkok",
    "MaleCanSee": true,
    "FemaleCanSee": true,
    "TransCanSee": true,
    "NeedReceiptValidate": false,
    "NeedUploadPhoto": false,
    "AccountNeedsVerification": false,
    "IsLiked": false,
    "BlockStatus": "2",
    "UsersList": null,
    "PhotoList": [
        {
            "UserID": 678,
            "PhotoID": 1076,
            "PhotoURL": null,
            "CreatedDate": "2018-09-21T15:07:05.550",
            "Order": 0
        },
        {
            "UserID": 678,
            "PhotoID": 1079,
            "PhotoURL": null,
            "CreatedDate": "2018-10-09T18:40:47.283",
            "Order": 0
        }
    ]
}
 */