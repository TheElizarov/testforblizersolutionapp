package com.dating.app.model.viewmodel

import android.arch.lifecycle.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel(), LifecycleObserver {
    private val compositeDisposable = CompositeDisposable()

    open fun subscribeTo(owner: LifecycleOwner) {
        owner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        compositeDisposable.clear()
    }

    protected fun request(request: Disposable) {
        compositeDisposable.add(request)
    }
}