package com.dating.app.repository

import android.content.Context
import com.dating.app.App
import com.dating.app.http.Progressable
import com.dating.app.model.pojo.Token
import com.dating.app.utils.request
import com.dating.app.utils.set

class TokenRepository : BaseRepository() {
    fun token(context: Context? = null,
              progressable: Progressable? = null,
              user: String,
              password: String,
              onSucces: ((Token) -> Unit)? = null,
              onError: ((Throwable) -> Unit)? = null) = request(
            App.instance
                    .api
                    .dating
                    .token(user, password)
                    .request(
                            context,
                            progressable,
                            onSuccess = {
                                it?.let {
                                    context?.set(it)
                                    onSucces?.invoke(it)
                                } ?: onError?.invoke(Exception("Authorization not correct, please, repeat request later"))
                            },
                            onError = {
                                onError?.invoke(it)
                            }
                    )
    )


}