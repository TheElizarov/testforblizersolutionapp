package com.dating.app.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import com.dating.app.R
import com.dating.app.http.Progressable
import com.dating.app.model.pojo.User
import com.dating.app.model.viewmodel.UsersViewModel
import com.dating.app.ui.fragment.UsersFragment.Adapter
import com.dating.app.utils.*
import kotlinx.android.synthetic.main.fragment_users.*
import kotlinx.android.synthetic.main.toolbar_with_search.*
import me.ilich.juggler.gui.JugglerFragment
import me.ilich.juggler.states.ContentOnlyState
import me.ilich.juggler.states.VoidParams
import org.jetbrains.anko.find

/**
 * Screen for search users [User]
 * It call HTTP request by [UsersViewModel.getUsers]
 * and display it on grid with [Adapter]
 * It automatic update user list, if user do not call request manualy more than
 * [UsersViewModel.INTERVAL_FOR_UPDATE_USERS_LIST_IN_MILLS], please, see [UsersViewModel.isNeedUpdate]
 */
class UsersFragment : RequestableFragment() {
    private val adapter = Adapter()

    private val usersViewModel: UsersViewModel by lazy {
        ViewModelProviders.of(this)
                .get(UsersViewModel::class.java)
                .apply {
                    subscribeTo(this@UsersFragment)
                }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_users_progressable, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareTopbar()
        setHasOptionsMenu(true)
        initUI()
        prepareViewModels()
        /**
         * update automatic,
         * if long not update by user manual
         */
        interval {
            refreshContainerSRL?.let {
                if (usersViewModel.isNeedUpdate()) {
                    context?.debug {
                        Log.v("UserFragment", "automatic update user list")
                    }
                    loadUsers(it.getProgressable())
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        loadUsers(progressable)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.filter, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                R.id.menu_filter -> activity?.displayMessage("In development")
                else -> Unit
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUI() {
        prepareTopbar()
        listTV.layoutManager = GridLayoutManager(context, 3)
        listTV.adapter = adapter
        listTV.pagination {
            refreshContainerSRL?.let {
                loadUsers(it.getProgressable())
            }
        }
        refreshContainerSRL?.let {
            it.setOnRefreshListener {
                usersViewModel.resetPagination()
                adapter.clear()
                loadUsers(it.getProgressable())
            }
        }
    }

    private fun prepareTopbar() = activity?.let {
        if (it is AppCompatActivity) {
            it.setSupportActionBar(customToolbar)
            filterET.filter { }
        }
    }

    private fun prepareViewModels() = with(usersViewModel) {
        usersLD.observe(this@UsersFragment, Observer {
            it?.let {
                adapter.add(it)
            }
        })
        errorLD.observe(this@UsersFragment, Observer {
            activity?.displayError("Ooh, it is error, try it later again..\n$it")
        })
    }

    private fun loadUsers(progressable: Progressable) = context?.let {
        checkInternetAvailable(progressable) {
            usersViewModel.getUsers(
                    it,
                    progressable
            )
        }
    }

    override fun onReloadClick() {
        super.onReloadClick()
        loadUsers(progressable)
    }

    class Adapter(private val content: MutableList<User> = mutableListOf()) : RecyclerView.Adapter<Adapter.Holder>() {
        override fun onCreateViewHolder(parent: ViewGroup, position: Int) = Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val value = content[position]
            with(holder) {
                with(nameTV) {
                    text = value.firstName
                    setCompoundDrawablesWithIntrinsicBounds(
                            if (value.isOnline) R.drawable.circle_green_layers else 0,
                            0,
                            0,
                            0)
                }
                descriptionTV.text = with(value) { "$age, $city" }
                previewIV.fromNetwork(
                        value.profileUrl,
                        placeHolderResId = R.drawable.female_white,
                        errorResId = R.drawable.female_white)
            }
        }

        override fun getItemCount() = content.size

        fun set(newContent: List<User>) {
            content.clear()
            add(newContent)
        }

        fun add(newContent: List<User>, onlyUnique: Boolean = true) {
            if (onlyUnique) {
                newContent.forEach {
                    if (!content.contains(it))
                        content.add(it)
                }
            } else content.addAll(newContent)
            notifyDataSetChanged()
        }

        fun clear() {
            content.clear()
            notifyDataSetChanged()
        }

        class Holder(item: View) : RecyclerView.ViewHolder(item) {
            val previewIV = item.find<ImageView>(R.id.previewIV)
            val nameTV = item.find<TextView>(R.id.nameTV)
            val descriptionTV = item.find<TextView>(R.id.descriptionTV)
        }
    }

    class State : ContentOnlyState<VoidParams>(VoidParams.instance()) {
        override fun onConvertContent(params: VoidParams?, fragment: JugglerFragment?) = instance()
    }

    companion object {
        fun instance() = UsersFragment()
    }
}