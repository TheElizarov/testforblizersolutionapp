package com.dating.app.ui.fragment.base

import android.os.Bundle
import android.support.v7.app.ActionBar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dating.app.R
import me.ilich.juggler.gui.JugglerFragment
import me.ilich.juggler.gui.JugglerToolbarFragment

class CommonToolbarFragment : JugglerToolbarFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_common_toolbar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun getToolbarId(): Int {
        return R.id.toolbar
    }

    companion object {

        fun instanceWithoutBack(): CommonToolbarFragment {
            return CommonToolbarFragment()
        }

        fun instance(): CommonToolbarFragment {
            val result = CommonToolbarFragment()
            val b = Bundle()
            JugglerToolbarFragment.addDisplayOptionsToBundle(b, ActionBar.DISPLAY_HOME_AS_UP or ActionBar.DISPLAY_SHOW_TITLE)
            result.arguments = b
            return result
        }

        fun createNavigation(): JugglerFragment {
            val f = CommonToolbarFragment()
            val b = JugglerToolbarFragment.addDisplayOptionsToBundle(null, ActionBar.DISPLAY_HOME_AS_UP or ActionBar.DISPLAY_SHOW_TITLE)
            f.arguments = b
            return f
        }

        fun createBack(): JugglerFragment {
            val f = CommonToolbarFragment()
            val b = JugglerToolbarFragment.addDisplayOptionsToBundle(null, ActionBar.DISPLAY_SHOW_TITLE or ActionBar.DISPLAY_HOME_AS_UP)
            f.arguments = b
            return f
        }
    }


}
