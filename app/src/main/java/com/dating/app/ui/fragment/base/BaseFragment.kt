package com.dating.app.ui.fragment.base

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import me.ilich.juggler.gui.JugglerFragment
import java.util.concurrent.TimeUnit

abstract class BaseFragment : JugglerFragment() {
    private val compositeDisposable = CompositeDisposable()

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun timer(seconds: Int,
              timeUnit: TimeUnit = TimeUnit.SECONDS,
              onTick: ((Int) -> Unit)? = null,
              onComplete: (() -> Unit)? = null) {
        compositeDisposable.add(
                Observable
                        .interval(1, timeUnit)
                        .take(seconds.toLong())
                        .map { seconds - it }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            if (it == 1L) onComplete?.invoke()
                            else onTick?.invoke(it.toInt())
                        }
        )
    }

    fun interval(timeUnit: TimeUnit = TimeUnit.SECONDS,
                 onTick: ((Int) -> Unit)? = null) {
        compositeDisposable.add(
                Observable
                        .interval(1, timeUnit)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            onTick?.invoke(it.toInt())
                        }
        )
    }


}