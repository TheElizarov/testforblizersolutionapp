package com.dating.app.ui.fragment

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dating.app.R
import com.dating.app.ui.fragment.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_navigation.*
import me.ilich.juggler.gui.JugglerFragment
import me.ilich.juggler.states.ContentOnlyState
import me.ilich.juggler.states.VoidParams

/**
 * Main navigation screen
 * Work after success auth
 * It has bottom navigation menu, when user click on menu item apply [NavigationFragment.set]
 */
class NavigationFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) = inflater.inflate(R.layout.fragment_navigation, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(navigationBNV) {
            enableAnimation(false)
            enableShiftingMode(false)
            enableItemShiftingMode(false)
            if (this is BottomNavigationView) setOnNavigationItemSelectedListener {
                when (it.getItemId()) {
                    R.id.menu_me -> set()
                    R.id.menu_find -> set(UsersFragment.instance())
                    R.id.menu_notification -> set()
                    R.id.menu_chat -> set()
                    else -> Unit
                }
                true
            }
        }
        set()
    }

    private fun set(fragment: Fragment = EmptyFragment.instance()) = activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.contentContainerLL, fragment)?.commit()

    class State : ContentOnlyState<VoidParams>(VoidParams.instance()) {
        override fun onConvertContent(params: VoidParams?, fragment: JugglerFragment?) = instance()
    }

    companion object {
        fun instance() = NavigationFragment()
    }
}