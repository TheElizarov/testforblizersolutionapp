package com.dating.app.ui.fragment

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.dating.app.R
import com.dating.app.http.Progressable
import com.dating.app.ui.fragment.base.BaseFragment
import com.dating.app.utils.isInternetAvailable
import org.jetbrains.anko.find

/**
 * Base fragment for screen with HTTP requests
 * It wraps template work with UI widget, for example:
 * 1) no internet connection error
 * 2) process request
 * 3) empty content
 *
 * Today it in development yet
 */
abstract class RequestableFragment : BaseFragment() {
    var progressPB: ProgressBar? = null
    var contentContainerRR: View? = null

    var refreshContainerSRL: SwipeRefreshLayout? = null

    var connectionErrorContainerRR: View? = null
    var connectionErrorDescriptionTV: TextView? = null
    var connectionErrorPreviewIV: ImageView? = null
    var connectionErrorReloadTV: TextView? = null

    val progressable = object : Progressable {
        override fun begin() {
            progressPB?.visibility = View.VISIBLE
            contentContainerRR?.visibility = View.GONE
        }

        override fun end() {
            progressPB?.visibility = View.GONE
            contentContainerRR?.let {
                it.visibility = View.VISIBLE
                it.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fadein))
            }
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bind(view)
    }

    fun bind(view: View) = with(view) {
        progressPB = find(R.id.progressPB)
        contentContainerRR = find(R.id.contentContainerRR)
        refreshContainerSRL = find(R.id.refreshContainerSRL)
        connectionErrorContainerRR = find(R.id.connectionErrorContainerRR)
        connectionErrorDescriptionTV = find(R.id.connectionErrorDescriptionTV)
        connectionErrorPreviewIV = find(R.id.connectionErrorPreviewIV)
        connectionErrorReloadTV = find(R.id.connectionErrorReloadTV)
        connectionErrorReloadTV?.setOnClickListener { onReloadClick() }
    }

    fun displayNoInternetError() {
        contentContainerRR?.visibility = View.GONE
        connectionErrorContainerRR?.visibility = View.VISIBLE
    }

    fun displayContent() {
        contentContainerRR?.visibility = View.VISIBLE
        connectionErrorContainerRR?.visibility = View.GONE
        progressPB?.visibility = View.GONE
    }

    fun checkInternetAvailable(progressable: Progressable? = null,
                               request: () -> Unit) {
        if (isInternetAvailable()) {
            displayContent()
            request()
        }
        else {
            progressable?.end()
            displayNoInternetError()
        }
    }

    open fun onReloadClick() {

    }
}