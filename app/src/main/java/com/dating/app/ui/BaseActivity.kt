package com.dating.app.ui

import com.dating.app.ui.fragment.AuthFragment
import com.dating.app.ui.fragment.NavigationFragment
import com.dating.app.utils.hasToken
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import me.ilich.juggler.gui.JugglerActivity
import me.ilich.juggler.states.State
import java.util.concurrent.TimeUnit

class BaseActivity : JugglerActivity() {
    private val compositeDisposable = CompositeDisposable()

    /**
     * It call after start App
     * if app has [Token] it navigate user to main screen
     * else it navigate user to Sign In screen
     */
    override fun createState(): State<*> {
        return if (hasToken()) NavigationFragment.State()
        else AuthFragment.State()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun timer(seconds: Int, timeUnit: TimeUnit = TimeUnit.SECONDS, onTick: ((Int) -> Unit)? = null, onComplete: (() -> Unit)? = null) {
        compositeDisposable.add(
                Observable
                        .interval(1, timeUnit)
                        .take(seconds.toLong())
                        .map { seconds - it }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            if (it == 1L) onComplete?.invoke()
                            else onTick?.invoke(it.toInt())
                        }
        )
    }
}