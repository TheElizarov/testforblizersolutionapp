package com.dating.app.ui.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dating.app.R
import com.dating.app.http.DefaultProgressable
import com.dating.app.model.viewmodel.TokenViewModel
import com.dating.app.ui.BaseActivity
import com.dating.app.ui.fragment.base.BaseFragment
import com.dating.app.ui.fragment.base.CommonToolbarFragment
import com.dating.app.utils.displayError
import com.dating.app.utils.displayMessage
import kotlinx.android.synthetic.main.fragment_auth.*
import me.ilich.juggler.change.Add
import me.ilich.juggler.change.Remove
import me.ilich.juggler.gui.JugglerFragment
import me.ilich.juggler.states.ContentBelowToolbarState
import me.ilich.juggler.states.VoidParams

/**
 * Screen for implements sign in logic
 * It call HTTP request [TokenViewModel.auth] and save token in private storage Context.set(Token)
 */
class AuthFragment : BaseFragment() {
    private val  tokenViewModel: TokenViewModel by lazy {
        ViewModelProviders.of(this)
                .get(TokenViewModel::class.java)
                .apply {
                    subscribeTo(this@AuthFragment)
                }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_auth, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(tokenViewModel) {
            tokenLD.observe(this@AuthFragment, Observer {
                it?.let {
                    activity?.displayMessage("Success for auth!") {
                        navigateTo().state(Add.newActivity(NavigationFragment.State(), BaseActivity::class.java))
                        navigateTo().state(Remove.closeCurrentActivity())
                    }
                }
            })
            errorLD.observe(this@AuthFragment, Observer {
                activity?.displayError("Something wrong, try again later..")
            })
        }
        authB.setOnClickListener {
            if (isLoginAndPasswordCorrect()) auth()
            else activity?.displayError("Please, fill login and password fields and try again")
        }
        useTestAccountB.setOnClickListener {
            loginTV.setText("zbouismail+502@gmail.com")
            passwordTV.setText("password")
        }
    }

    fun isLoginAndPasswordCorrect() = loginTV.text.isNotEmpty() && passwordTV.text.isNotEmpty()

    fun auth() = activity?.let {
        tokenViewModel.auth(
                it,
                DefaultProgressable(it),
                loginTV.text.toString(),
                passwordTV.text.toString()

        )
    }

    class State : ContentBelowToolbarState<VoidParams>(VoidParams.instance()) {
        override fun onConvertContent(params: VoidParams?, fragment: JugglerFragment?)
                = instance()

        override fun onConvertToolbar(params: VoidParams?, fragment: JugglerFragment?)
                = CommonToolbarFragment.instanceWithoutBack()

    }

    companion object {
        fun instance() = AuthFragment()
    }
}