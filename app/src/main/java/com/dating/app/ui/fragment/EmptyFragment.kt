package com.dating.app.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.dating.app.R
import com.dating.app.ui.fragment.base.BaseFragment

/**
 * Use it for stub in development
 * Empty fragment with message in the center "IN DEVELOPMENT"
 */
class EmptyFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?)
            = inflater.inflate(R.layout.fragment_empty, container, false)

    companion object {
        fun instance() = EmptyFragment()
    }
}