package com.dating.app

import android.app.Application
import com.dating.app.http.api.Api

class App : Application() {
    lateinit var api : Api

    override fun onCreate() {
        super.onCreate()
        instance = this
        api = Api.init()
    }

    companion object {
        lateinit var instance: App
    }
}