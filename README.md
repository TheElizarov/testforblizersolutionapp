Android App for Chat
===============

# Useful links
* [Upwork](https://www.upwork.com/ab/f/contracts/21296389#time%2F20181123%2F20181222)

# Description
* IDE Android Studio 3+
* Gradle
* Kotlin
* Frameworks: Android SDK (fragments, ViewModel, LiveData, LifecycleHandling and others), Retrofit2, RxJava2 and others
* MVVM

# Tasks

## Sprint 1

### Description
 
- list of users with scrolling without filtering with bottom bar and navigation bar

- scrolling
 
- refresh the list after 60 seconds if the user is not scrolling it
 
- refresh if the user open the app again from the background

### Expected time

8-10 hours



